﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour {
    
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Fireball trigger: " + other.tag);

        if (other.tag == "Player")
        {
            other.GetComponent<PlayerController>().DealDamage(1);
        }

        if (other.tag == "Wall" || other.tag == "Ground" || other.tag == "Sand" || other.tag == "Player")
        {
            Destroy(gameObject);
        }
    }
}
