﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

    public static GameController instance;

    public GameObject gameOverText;
    public Button resumeButton;
    public GameObject pauseMenu;
    public Text scoreText;
    public bool gameOver;
    public GameObject player;
    public static bool isPaused = false;

    public Image hp1;
    public Image hp2;
    public Image hp3;

    public const float scrollSpeed = -2.5f;

    private int score = 0;
    private AudioSource music;

    public float musicMaxVolume = 0.2f;

    void Awake()
    {
        // Enforcing singleton pattern
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }

    void Start()
    {
        music = GetComponent<AudioSource>();

        gameOverText.SetActive(false);
        RefreshScore();
        Resume();
    }

    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (isPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }
        else if (Input.GetKeyDown(KeyCode.F11))
        {
            Restart();
        }
        else if (Input.GetKeyDown(KeyCode.F12))
        {
            Quit();
        }
    }

    public void Pause()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
        music.volume = musicMaxVolume / 3;
    }

    public void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        music.volume = musicMaxVolume;
    }

    public void Quit()
    {
        Resume();
        SceneManager.LoadScene("MainMenu");
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void PlayerDied() {
        Debug.Log("GameController: PlayerDied");
        gameOverText.SetActive(true);
        resumeButton.interactable = false;

        gameOver = true;
        Pause();
    }

    public void PlayerScored(int value)
    {
        if(gameOver)
        {
            return;
        }
        score += value;
        RefreshScore();
    }

    public void RefreshScore()
    {
        scoreText.text = "Score: " + score.ToString();
    }


    public void UpdateHp(int value)
    {
        toggleHpIndicator(hp1, value >= 1);
        toggleHpIndicator(hp2, value >= 2);
        toggleHpIndicator(hp3, value >= 3);
    }

    private void toggleHpIndicator(Image image, bool value)
    {
        Color color = image.color;
        color.a = value ? 1f : 0.2f;
        image.color = color;

    }
}
