﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

    public float upForce;
    public float speed = 2.2f;
    private bool isDead = false;
    public int hp = 3;

    public AudioClip clipTrap;
    public AudioClip clipStone;

    enum State { Idle, Left, Right, Down, Falling };
    private State state;
    private State nextState;

    private Vector3 targetPosition = Vector3.zero;

    private int worldLayerMask;

    private SwipeInput swipeInput;

    private Rigidbody rb;
    private AudioSource source;

    void Start () {
        rb = GetComponent<Rigidbody>();
        source = GetComponent<AudioSource>();

        transform.position = new Vector3(0f, 1f, 0f);
        state = State.Idle;
        rb.useGravity = true;
        //rb.constraints = RigidbodyConstraints.FreezeAll;

        worldLayerMask = 1 << LayerMask.NameToLayer("World");

        swipeInput = GetComponent<SwipeInput>();

        SetHp(hp);
    }
	
	void Update () {
        if (isDead == false && GameController.isPaused == false) {

            /*
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");
            if (horizontal != 0)
            {
                // turnSpeed * horizontal * Time.deltaTime);
                Debug.Log("horizontal " + horizontal);
            }

            if (vertical > 0)
            {
                Debug.Log("vertical " + vertical);
            }

            if (Input.GetButton("Fire1"))
            {
                Debug.Log("Fire1 ");
            }
            */
            SwipeInput.Gesture gesture = swipeInput.getGesture();

            if (Input.GetKeyDown("left") || gesture == SwipeInput.Gesture.Left)
            {
                //getZombieLeft();
                Debug.Log("Move left");
                nextState = State.Left;
            }
            if (Input.GetKeyDown("right") || gesture == SwipeInput.Gesture.Right)
            {
                //getZombieRight();
                Debug.Log("Move right");
                nextState = State.Right;
            }
            if (Input.GetKeyDown("down") || gesture == SwipeInput.Gesture.Down)
            {
                //PushUp();
                Debug.Log("Move down");
                nextState = State.Down;
            }

            if (state == State.Idle)
            {
                rb.useGravity = true;
            }

            if (state == State.Idle && nextState != State.Idle)
            {
                state = nextState;
                nextState = State.Idle;

                if (state == State.Left)
                {
                    RaycastHit hit;
                    Physics.Raycast(transform.position, Vector3.left, out hit, 1f, worldLayerMask);
                    if (hit.transform && hit.transform.tag == "Wall")
                    {
                        source.PlayOneShot(clipStone);
                        state = State.Idle;
                    }
                    else if (hit.transform && hit.transform.GetComponent<Destroyable>())
                    {
                        hit.transform.GetComponent<Destroyable>().hurt();
                    }
                    targetPosition = new Vector3(transform.position.x - 1, transform.position.y, 0);
                }
                else if (state == State.Right)
                {
                    RaycastHit hit;
                    Physics.Raycast(transform.position, Vector3.right, out hit, 1f, worldLayerMask);
                    if (hit.transform && hit.transform.tag == "Wall")
                    {
                        source.PlayOneShot(clipStone);
                        state = State.Idle;
                    }
                    else if (hit.transform && hit.transform.GetComponent<Destroyable>())
                    {
                        hit.transform.GetComponent<Destroyable>().hurt();
                    }
                    targetPosition = new Vector3(transform.position.x + 1, transform.position.y, 0);
                }
                else if (state == State.Down)
                {
                    RaycastHit hit;
                    Physics.Raycast(transform.position, Vector3.down, out hit, 1f, worldLayerMask);
                    if (hit.transform && (hit.transform.tag == "Wall" || hit.transform.tag == "Trap"))
                    {
                        source.PlayOneShot(clipStone);
                        state = State.Idle;
                    }
                    else if(hit.transform && hit.transform.GetComponent<Destroyable>())
                    {
                        hit.transform.GetComponent<Destroyable>().hurt();
                        rb.AddForce(0f, 100f, 0f);
                        rb.constraints = RigidbodyConstraints.FreezeAll ^ RigidbodyConstraints.FreezePositionY;
                    }
                    else
                    {
                        //
                    }
                }

                rb.useGravity = !(state == State.Left || state == State.Right);

                Debug.Log("Player state: " + state);
                Debug.Log("Player targetPosition: " + targetPosition);
            }

            if (state == State.Left || state == State.Right)
            {
                float step = speed * Time.deltaTime;
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);
                if (transform.position == targetPosition)
                {
                    state = State.Idle;
                    //rb.useGravity = true;
                }
            }
        }
	}

    private void OnCollisionEnter(Collision collision)
    {
        PickUp pickUp = collision.gameObject.GetComponent<PickUp>();

        if (pickUp != null)
        {
            Debug.Log("xxx " + pickUp.ToString());

            pickUp.pickUp();
        }

        if (state == State.Down)
        {

            if(collision.gameObject.tag == "Respawn")
            {
                GameController.instance.Restart();
            }

            if (worldLayerMask == (1 << collision.gameObject.layer))
            {
                state = State.Idle;
                //rb.constraints = RigidbodyConstraints.FreezeAll;
                //rb.velocity = Vector3.zero;
            }
        }
    }

    public void DealDamage(int value) {
        int newHp = hp - value;
        if (newHp < 0) {
            newHp = 0;
        }
        SetHp(newHp);
    }

    private void SetHp(int value) {
        hp = value;
        if (hp == 0)
        {
            isDead = true;
            GameController.instance.PlayerDied();
        }
        GameController.instance.UpdateHp(hp);
    }

    private void OnTriggerEnter(Collider other)
    {

        PickUp pickUp = other.GetComponent<PickUp>();

        if (pickUp != null)
        {
            pickUp.pickUp();
        }

        if (other.tag == "Trap")
        {
            source.PlayOneShot(clipTrap);
            DealDamage(1);
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x - 0.75f, transform.position.y, 0f));
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x + 0.75f, transform.position.y, 0f));
    }

}
