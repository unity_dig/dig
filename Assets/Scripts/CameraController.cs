﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public float speed = 1f;

    private GameObject player;

    // Use this for initialization
    void Start () {
        player = GameController.instance.player;

    }
	
	// Update is called once per frame
	void Update ()
    {
        float step = speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(0f, player.transform.position.y, transform.position.z), step);
    }
}
