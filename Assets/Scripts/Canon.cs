﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Canon : MonoBehaviour {

    public float initialDelay = 0f;
    public float interval = 4f;
    public float projectileSpeed = 3f;
    public GameObject projectilePrefab;
	
	void Start () {
        StartCoroutine(FireDelay());
    }
	
	void Update () {
		
	}

    IEnumerator FireDelay()
    {
        yield return new WaitForSeconds(initialDelay);
        StartCoroutine(FireLoop());
    }

    IEnumerator FireLoop()
    {
        while(true)
        {
            Debug.Log("Fire!");
            Vector3 position = transform.position + transform.rotation * new Vector3(0f, 0.2f, 0f);
            GameObject projectile = Instantiate(projectilePrefab, position, Quaternion.identity);
            projectile.GetComponent<Rigidbody>().isKinematic = false;
            projectile.GetComponent<Rigidbody>().velocity = transform.rotation * new Vector3(0f, projectileSpeed, 0f);
            yield return new WaitForSeconds(interval);
        }
    }
}
