﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeInput : MonoBehaviour {
    private Vector3 startPosition = Vector3.zero;
    private Vector3 endPosition = Vector3.zero;

    public enum Gesture { None, Left, Right, Up, Down };
    private Gesture gesture = Gesture.None;

    private float magnitude = 0.2f;
    private float tolerance = 0.1f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        gesture = Gesture.None;

        if (Input.GetMouseButtonDown(0))    // swipe begins
        {
            //startPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            startPosition = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))    // swipe ends
        {
            //endPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            endPosition = Input.mousePosition;
        }

        if (startPosition != endPosition && startPosition != Vector3.zero && endPosition != Vector3.zero)
        {
            float scale = Mathf.Min(Screen.width, Screen.height);
            Debug.Log("startPosition " + startPosition + " endPosition" + endPosition + " scale " + scale);

            float deltaX = (endPosition.x - startPosition.x) / scale;
            float deltaY = (endPosition.y - startPosition.y) / scale;
            Debug.Log("deltaX " + deltaX + " deltaY" + deltaY);

            if ((deltaX > magnitude || deltaX < -magnitude) && (deltaY >= -tolerance  || deltaY <= tolerance))
            {
                if (startPosition.x < endPosition.x)
                {
                    gesture = Gesture.Right;
                }
                else
                {
                    gesture = Gesture.Left;
                }
            }
            else if ((deltaY > magnitude || deltaY < -magnitude) && (deltaX >= -tolerance || deltaX <= tolerance))
            {
                if (startPosition.y < endPosition.y)
                {
                    gesture = Gesture.Up;
                }
                else
                {
                    gesture = Gesture.Down;
                }
            }
            startPosition = endPosition = Vector3.zero;
        }
    }

    public Gesture getGesture() {
        return gesture;
    }
}
