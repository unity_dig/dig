﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour {

    public int value = 1;
    public GameObject spawnOnPickup;

    void Start () {
		
	}

    public void pickUp()
    {
        Instantiate(spawnOnPickup, transform.position, Quaternion.identity);
        GameController.instance.PlayerScored(value);
        Destroy(gameObject);
    }

}
