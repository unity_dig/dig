﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sand : MonoBehaviour {

    public GameObject spawnOnDestroy;
    private float ttl = 1.2f;

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            Touch();
        }
    }

    public void Touch()
    {
        Debug.Log("Sand: Touch");
        StartCoroutine(WaitAndDestroy());
    }

    IEnumerator WaitAndDestroy()
    {
        yield return new WaitForSeconds(ttl);
        Instantiate(spawnOnDestroy, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

}
