﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour {

    public GameObject spawnOnDestroy;

    // Use this for initialization
    void Start () {
        
    }

    // Update is called once per frame
    void Update () {
		
	}

    public int hurt()
    {
        Instantiate(spawnOnDestroy, transform.position, Quaternion.identity);
        Destroy(gameObject);
        return 0;
    }
}
